# Class: banner
# Installs and generates a /etc/banner for use with SSH
# Use as:
# class { cms::banner: loc => "AMS", remark => "None!" }

class banner ($loc = "", $remark = "") {
    $banner = $::osfamily ? {
        "RedHat"  => "/etc/banner",
        default   => "/etc/banner",
    }
    file { "/etc/banner":
        path    => $banner,
        content => template("banner/banner.erb"),
    }
}
