# vi: set sw=2 ts=2 ai et:

# Class: resolv::config
class resolv::config {
  file { $::resolv::config_file :
    ensure  => file,
    content => template($::resolv::config_template),
    path    => $::resolv::config_file,
    owner   => $::resolv::config_user,
    group   => $::resolv::config_group,
    mode    => $::resolv::config_mode,
  }
}
