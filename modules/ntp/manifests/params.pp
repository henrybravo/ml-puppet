# Class: ntp
class ntp::params {

  # Some defaults
  # TODO: Because of bug 20923, the default values for 'peer' and 'allow'
  #       are the empty string, and not 'undef'. When bug 20923 is resolved
  #       change this to undef, and change the template file the way the
  #       documentation shows.
  $servers = [ '0.pool.ntp.net', '1.pool.ntp.net']
  $master  = false
  $peer    = ''
  $allow   = ''

  # Settings per OS
  case $::osfamily {
    'RedHat' : {
      $config_file        = '/etc/ntp.conf'
      $config_template    = "${module_name}/ntp.conf.erb"
      $config_user        = 'root'
      $config_group       = 'root'
      $config_mode        = '0644'
      $pkg_list           = 'ntp'
      $pkg_ensure         = latest
      $service_ensure     = running
      $service_enable     = true
      $service_hasstatus  = true
      $service_hasrestart = true
      $service_name       = 'ntpd'
    }

    'Debian' : {
      $config_file        = '/etc/ntp.conf'
      $config_template    = "${module_name}/ntp.conf.erb"
      $config_user        = 'root'
      $config_group       = 'root'
      $config_mode        = '0644'
      $pkg_list           = 'ntp'
      $pkg_ensure         = present
      $service_ensure     = running
      $service_enable     = true
      $service_hasstatus  = true
      $service_hasrestart = true
      $service_name       = 'ntp'
    }

    'FreeBSD' : {
      $config_file        = '/usr/local/etc/ntp.conf'
      $config_template    = "${module_name}/ntp.conf.erb"
      $config_user        = 'root'
      $config_group       = 'wheel'
      $config_mode        = '0644'
      $pkg_list           = 'ntp'
      $pkg_ensure         = present
      $service_ensure     = running
      $service_enable     = true
      $service_hasstatus  = false
      $service_hasrestart = false
      $service_name       = 'ntpd'
    }

    default: {
      fail("Module '${module_name}': ${::osfamily} v ${::lsbmajdistrelease} not supported!")
    }
  }
}
