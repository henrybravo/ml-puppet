# Class: ntp::install
class ntp::install {
  package { $::ntp::pkg_list :
    ensure => $::ntp::pkg_ensure,
  }
}
