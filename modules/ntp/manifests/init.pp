# Class: ntp
class ntp (
    $servers            = $::ntp::params::servers,
    $master             = $::ntp::params::master,
    $peer               = $::ntp::params::peer,
    $allow              = $::ntp::params::allow,
    $config_file        = $::ntp::params::config_file,
    $config_template    = $::ntp::params::config_template,
    $config_user        = $::ntp::params::config_user,
    $config_group       = $::ntp::params::config_group,
    $config_mode        = $::ntp::params::config_mode,
    $pkg_list           = $::ntp::params::pkg_list,
    $pkg_ensure         = $::ntp::params::pkg_ensure,
    $service_ensure     = $::ntp::params::service_ensure,
    $service_enable     = $::ntp::params::service_enable,
    $service_hasstatus  = $::ntp::params::service_hasstatus,
    $service_hasrestart = $::ntp::params::service_hasrestart,
    $service_name       = $::ntp::params::service_name,
  ) inherits ntp::params {

  include ntp::install
  include ntp::config
  include ntp::service

  Class['ntp::install']
    -> Class['ntp::config']
    -> Class['ntp::service']
}
