# vi: set sw=2 ts=2 ai et:
class baseline {
  class { 'baseline::banner' : }
  class { 'baseline::ntp' : }
  class { 'baseline::resolv' : }
  class { 'baseline::ssh' : }
}
