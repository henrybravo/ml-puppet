# vi: set sw=2 ts=2 ai et:

# Super Class: default::banner
class baseline::banner ($loc = "", $remark = "") {
  class { '::banner' :
    loc    => $loc,
    remark => $remark,
  }
}
